# -*- coding: utf-8 -*- 
from pylab import *
from camera import Camera
from sfm import *
#假设数据
K=array([[1,0,0],[0,1,0],[0,0,1]])
#X=array([[2,10,15,30,3,5,6,7,90],[3,0,20,30,20,10,30,40,20],[1,20,10,20,2,3,4,5,70],[1,1,1,1,1,1,1,1,1]],dtype=float)
n=100
X=vstack((random.randn(3,n)*100,ones((1,n))))
P1=array([[1,0,0,0],[0,1,0,0],[0,0,1,0]],dtype=float)
P2=array([[1,0,0,10],[0,1,0,10],[0,0,1,0]],dtype=float)
P3=array([[1,0,0,20],[0,1,0,5],[0,0,1,0]],dtype=float)
c1=Camera(dot(K,P1))
c2=Camera(dot(K,P2))
c3=Camera(dot(K,P3))
x1=c1.project(X) #x1=K*P1*X
x2=c2.project(X) #x2=K*P2*X
x3=c3.project(X) #x3=K*P3*X
#开始计算
x1n=dot(inv(K),x1) #x1n=K^-1*x1
x2n=dot(inv(K),x2) #x2n=K^-1*x2
x3n=dot(inv(K),x3)
E=compute_fundamental_normalized(x1n[:,:8], x2n[:,:8]) 
P2_E_ALL=compute_P_from_essential(E)
P2_E=choose_P(x1n, x2n, P1, P2_E_ALL)
X12_E=triangulate(x1n, x2n, P1,P2_E)
P3_E=compute_P(x3[:,:6],X12_E[:,:6])
c3_E=Camera(P3_E)
print "P3_E"
print P3_E
X23_E=triangulate(x2n, x3n, P2_E,P3_E)
print "|X12_E-X23_E|"
print norm(X12_E-X23_E)
X13_E=triangulate(x1n, x3n, P1,P3_E)
print "|X12_E-X13_E|"
print norm(X12_E-X13_E)
print "|x3-P3_E*X23_E|"
print norm(x3-c3_E.project(X23_E))
import H3D
H=H3D.H_from_points(X12_E[:,:5],X[:,:5])
print "|X-H*X23_E|"
print norm(X-dot(H,X23_E))
print "|P3-P3_E*H^-1|"
P3_E=dot(P3_E,inv(H))
print norm(P3/norm(P3)-P3_E/norm(P3_E))