# -*- coding: utf-8 -*- 
from pylab import *
from camera import Camera
from sfm import *
#假设数据
K=array([[1,0,0],[0,1,0],[0,0,1]])
X=array([[2,10,15,30,3,5,6,7],[3,0,20,30,20,10,30,40],[1,20,10,20,2,3,4,5],[1,1,1,1,1,1,1,1]],dtype=float)
P1=array([[1,0,0,0],[0,1,0,0],[0,0,1,0]],dtype=float)
P2=array([[1,0,0,10],[0,1,0,10],[0,0,1,0]],dtype=float)
c1=Camera(dot(K,P1))
c2=Camera(dot(K,P2))
x1=c1.project(X) #x1=K*P1*X
x2=c2.project(X) #x2=K*P2*X
#开始计算
x1n=dot(inv(K),x1) #x1n=K^-1*x1
x2n=dot(inv(K),x2) #x2n=K^-1*x2
E=compute_fundamental_normalized(x1n, x2n) #x2^T*F*x1=x2n^T*E*x1n=0, E=K^T*F*K
print "Essential Matrix"
print E
print "|x2n^T*E*x1n|"
for i in range(8):
    print dot(dot(x2n[:,i].T,E),x1n[:,i])
P2_E_ALL=compute_P_from_essential(E)
print "P2_E_ALL"
print P2_E_ALL
P2_E=choose_P(x1n, x2n, P1, P2_E_ALL)
print "P2_E"
print P2_E
X_E=triangulate(x1n, x2n, P1,P2_E)
print "X_E"
print X_E
print "|x1-P1*X_E|"
print norm(x1-c1.project(X_E))
c2_e=Camera(P2_E)
print "|x2-P2_E*X_E|"
print norm(x2-c2_e.project(X_E))
print "|X-X_E|"
print norm(X-X_E)
print "|P2-P2_E|"
print norm(P2-P2_E)
import H3D
H=H3D.H_from_points(X_E[:,:5],X[:,:5])
print "H"
print H
print "H*X_E"
print dot(H,X_E)
print "|X-H*X_E|"
print norm(X-dot(H,X_E))
invH=H3D.H_from_points(X[:,:5],X_E[:,:5])
print "H^-1"
print invH
print "P2_E*H^-1"
print dot(P2_E,invH)
print "|P2-P2_E*H^-1|"
print norm(P2-dot(P2_E,invH))
A=dot(P2_E,invH)
A=A/norm(A)
P2=P2/norm(P2)
print norm(P2-A)