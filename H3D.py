from pylab import *
import scipy.optimize as opt
import numpy.random as random
def H_from_points(X1,X2):
    A=zeros((15,16))
    for i in range(5):
        x1=X1[0][i]
        y1=X1[1][i]
        z1=X1[2][i]
        x2=X2[0][i]
        y2=X2[1][i]
        z2=X2[2][i]
        A[i*3]=[x1,y1,z1,1,0,0,0,0,0,0,0,0,-x2*x1,-x2*y1,-x2*z1,-x2]
        A[i*3+1]=[0,0,0,0,x1,y1,z1,1,0,0,0,0,-y2*x1,-y2*y1,-y2*z1,-y2]
        A[i*3+2]=[0,0,0,0,0,0,0,0,x1,y1,z1,1,-z2*x1,-z2*y1,-z2*z1,-z2]
    U,S,V = linalg.svd(A)
    H = V[-1].reshape(4,4)
    return H/H[3,3]
    #return H
def test():
    X1=vstack((random.randn(3,5),ones((1,5))))
    H=array([[1,0,0,0],[0,1,0,10],[0,0,1,0],[0,0,0,1]])
    X2=dot(H,X1)
    print X1
    print X2
    H=H_from_points(X1, X2)
    print dot(H,X1)
    print norm(X2-dot(H,X1))
if __name__=='__main__':
    test()     
